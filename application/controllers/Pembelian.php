<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pembelian extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("Pembelian_model");
	$this->load->model("Supplier_model");
	$this->load->model("Barang_model");
}
	public function index()
{
	$this->ListPembelian();
}
public function InputPembelian()
{
	$data['data_supplier']= $this->Supplier_model->tampilDataSupplier();
	$data['content'] = 'input/input_pembelian';
	//if (!empty($_REQUEST)) {
		//$pembelian_header->savePembelianHeader();
		
		
		$validation = $this->form_validation;
		$validation->set_rules($this->Pembelian_model->rules());
		if ($validation->run()){
		$pembelian_header = $this->Pembelian_model;
		$this->Pembelian_model->savePembelianHeader();
		$id_terakhir = $pembelian_header->idTransaksiTerakhir();
		redirect("pembelian/InputPembelianDetail/" . $id_terakhir, "refresh");
		 }
	$this->load->view('home-2',$data);
	}
	public function InputPembelianDetail($id_pembelian_header)
{
	$data['data_barang']= $this->Barang_model->tampilDataBarang();
	$data['id_header']= $id_pembelian_header;
	$data['data_pembelian_detail']= $this->Pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
	$data['content'] = 'input/input_pembelian_d';
	//if (!empty($_REQUEST)) {
		$validation = $this->form_validation;
		$validation->set_rules($this->Pembelian_model->rules());

		if ($validation->run()){
		$m_pembelian = $this->Pembelian_model;
		$this->Pembelian_model->savePembelianDetail($id_pembelian_header);
		$kode_barang = $this->input->post('kode_barang');
		$qty = $this->input->post('qty');
		$this->barang_model->updateStok($kode_barang, $qty);
		
		//redirect("pembelian/Inputdetail/" . $id_pembelian_header, "refresh");
		 }
	$this->load->view('home-2',$data);
	}
	public function ListPembelian()
{
	$data['data_pembelian']= $this->Pembelian_model->tampilDataPembelian();
	$data['content'] = 'form/list_pembelian';
	$this->load->view('home-2',$data);
}
	}