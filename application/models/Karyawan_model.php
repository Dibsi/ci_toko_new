<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model{
	private $_table = "karyawan";

	public function rules()
	{
		return[
		[
			'field' 		=> 'nik',
			'label' 		=> 'Nik',
			'rules' 		=> 'required[max_length[5]',
			'errors'		=>[
			'required' 		=> 'Nik Tidak Boleh Kosong.',
			'max_length'	=> 'Nik Tidak Boleh lebih dari 5 karakter.',
			],
		],
		[
			'field' 		=> 'nama_karyawan',
			'label' 		=> 'Nama Karyawan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Karyawan Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tempat_lahir',
			'label' 		=> 'Tempat Lahir',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tempat Lahir Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tgl','bln','thn',
			'label' 		=> 'Tanggal Bulan tahun',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tanggal bulan Tahun Tidak Boleh Kosong.',
			],
		[
			'field' 		=> 'jenis_kelamin',
			'label' 		=> 'Jenis Kelamin',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Jenis Kelamin Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'alamat',
			'label' 		=> 'Alamat',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Alamat Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'telp',
			'label' 		=> 'Telpon',
			'rules' 		=> 'required|numeric',
			'errors'		=> [
			'required' 		=> 'Telpon Tidak Boleh Kosong.',
			'numeric' 		=> 'Telpon Harus Angka.',
			],
		],
		[
			'field' 		=> 'kode_jabatan',
			'label' 		=> 'Kode Jabatan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Kode Jabatan Tidak Boleh Kosong.',
			],
		],
		]
		];
	}

	public function TampilDataKaryawan()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataKaryawan2()
	{
	$queery = $this->db->querry("select * from karyawan WHERE flag = 1");
	return $querry->result();
	}

	public function TampilDataKaryawan3()
	{
	$this->db->select('*');
	$this->db->order_by('nik','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}
	public function  save()
	{
	$tgl = $this->input->post('tgl');
	$bln = $this->input->post('bln');
	$thn = $this->input->post('thn');
	$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
	
	$data['nik']			= $this->input->post('nik');
	$data['nama_lengkap']	= $this->input->post('nama_karyawan');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tgl_lahir']		= $tgl_gabung;
	$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['kode_jabatan']	= $this->input->post('kode_jabatan');
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($nik)
	{
	$tgl = $this->input->post('tgl');
	$bln = $this->input->post('bln');
	$thn = $this->input->post('thn');
	$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
	
	$data['nama_lengkap']	= $this->input->post('nama_karyawan');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tgl_lahir']		= $tgl_gabung;
	$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['kode_jabatan']	= $this->input->post('kode_jabatan');
	$data['flag']			= 1;
	$this->db->where('nik', $nik);
	$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
}