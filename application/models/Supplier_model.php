<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model{
	private $_table = "supplier";

	public function rules()
	{
		return[
		[
			'field' 		=> 'kode_supplier',
			'label' 		=> 'Kode Supplier',
			'rules' 		=> 'required[max_length[5]',
			'errors'		=>[
			'required' 		=> 'Kode Supplier Tidak Boleh Kosong.',
			'max_length'	=> 'Kode Supplier Tidak Boleh lebih dari 5 karakter.',
			],
		],
		[
			'field' 		=> 'nama_supplier',
			'label' 		=> 'Nama Supplier',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Supplier Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'alamat',
			'label' 		=> 'Alamat',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Alamat Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'telp',
			'label' 		=> 'Telpon',
			'rules' 		=> 'required|numeric',
			'errors'		=> [
			'required' 		=> 'Nama Jenis Tidak Boleh Kosong.',
			'numeric' 		=> 'Telpon harus Angka.',
			],
		]
		];
	}

	public function TampilDataSupplier()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataSupplier2()
	{
	$queery = $this->db->querry("select * from supplier WHERE flag = 1");
	return $querry->result();
	}

	public function TampilDataSupplier3()
	{
	$this->db->select('*');
	$this->db->order_by('kode_supplier','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}
	public function  save()
	{
	$data['kode_supplier']	= $this->input->post('kode_supplier');
	$data['nama_supplier']	= $this->input->post('nama_supplier');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($kode_supplier)
	{
	$data['kode_supplier']	= $this->input->post('kode_supplier');
	$data['nama_supplier']	= $this->input->post('nama_supplier');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['flag']			= 1;
	$this->db->where('kode_supplier', $kode_supplier);
	$this->db->update($this->_table, $data);
	}
	public function delete($kode_supplier)
	{
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);
	}
}
