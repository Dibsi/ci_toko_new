<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_model extends CI_Model{
	private $_table = "jabatan";

	public function rules()
	{
		return[
		[
			'field' 		=> 'id_jabatan',
			'label' 		=> 'Kode Jabatan',
			'rules' 		=> 'required[max_length[5]',
			'errors'		=>[
			'required' 		=> 'Kode Jabatan Tidak Boleh Kosong.',
			'max_length'	=> 'Kode Jabatan Tidak Boleh lebih dari 5 karakter.',
			],
		],
		[
			'field' 		=> 'nama_jabatan',
			'label' 		=> 'Nama Jabatan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Jabatan Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'keterangan',
			'label' 		=> 'keterangan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'keterangan Tidak Boleh Kosong.',
			],
		]
		];
	}

	public function TampilDataJabatan()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataJabatan2()
	{
	$queery = $this->db->query("select * from jabatan WHERE flag = 1");
	return $query->result();
	}

	public function TampilDataJabatan3()
	{
	$this->db->select('*');
	$this->db->order_by('kode_jabatan','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}
	public function  save()
	{
	$data['kode_jabatan']	= $this->input->post('id_jabatan');
	$data['nama_jabatan']	= $this->input->post('nama_jabatan');
	$data['keterangan']		= $this->input->post('keterangan');
	$data['flag']			= 1;

	$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($kode_jabatan)
	{
	$data['kode_jabatan']	= $this->input->post('id_jabatan');
	$data['nama_jabatan']	= $this->input->post('nama_jabatan');
	$data['keterangan']		= $this->input->post('keterangan');
	$data['flag']			= 1;
	$this->db->where('kode_jabatan', $kode_jabatan);
	$this->db->update($this->_table, $data);
	}
	public function delete($kode_jabatan)
	{
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->delete($this->_table);
	}
}