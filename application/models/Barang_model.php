<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model{
	private $_table = "barang";

	public function rules()
	{
		return[
		[
			'field' 		=> 'kode_barang',
			'label' 		=> 'Kode Barang',
			'rules' 		=> 'required[max_length[5]',
			'errors'		=>[
			'required' 		=> 'Kode Barang Tidak Boleh Kosong.',
			'max_length'	=> 'Kode barang Tidak Boleh lebih dari 5 karakter.',
			],
		],
		[
			'field' 		=> 'nama_barang',
			'label' 		=> 'Nama Barang',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Barang Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'harga_barang',
			'label' 		=> 'Harga Barang',
			'rules' 		=> 'required|numeric',
			'errors'		=> [
			'required' 		=> 'Harga Barang Tidak Boleh Kosong.',
			'numeric'		=> 'Harga Barang Harus Angka.',
			],
		],
		[
			'field' 		=> 'kode_jenis',
			'label' 		=> 'Kode Jenis',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Jenis Tidak Boleh Kosong.',
			],
		]
		];
	}

	public function TampilDataBarang()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataBarang2()
	{
	$query = $this->db->query("select * from jenis_barang as jb inner join barang as b on jb.kode_jenis=b.kode_jenis");
	return $query->result();
	}

	public function TampilDataBarang3()
	{
	$this->db->select('*');
	$this->db->order_by('kode_barang','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}
	public function  save()
	{
	$data['kode_barang']	= $this->input->post('kode_barang');
	$data['nama_barang']	= $this->input->post('nama_barang');
	$data['harga_barang']	= $this->input->post('harga_barang');
	$data['kode_jenis']		= $this->input->post('kode_jenis');
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($kode_barang)
	{
	$data['kode_barang']	= $this->input->post('kode_barang');
	$data['nama_barang']	= $this->input->post('nama_barang');
	$data['harga_barang']	= $this->input->post('harga_barang');
	$data['kode_jenis']		= $this->input->post('kode_jenis');
	$data['flag']			= 1;
	$this->db->where('kode_barang', $kode_barang);
	$this->db->update($this->_table, $data);
	}
	public function delete($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
}